public class FindIndex {
    public static void main(String[] arg){
        int[] array = {1, 15, 42, 3, 27};
        int i, key=273;
        for(i=0;i<array.length;i++){
            if(array[i]==key) {
                System.out.println("index of " + key + " in this array is " + i);
                break;
            }
        }
        if(i==array.length)
            System.out.println("there is no such element");

    }
}
