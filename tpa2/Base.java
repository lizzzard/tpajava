public class Base {
    public void method1(){
        System.out.println("base method1 called");
    }
    void method2(){
        System.out.println("base method2 called");
    }
    protected void method3(){
        System.out.println("base method3 called");
    }
    protected void method4(){
        System.out.println("base method4 called");
    }
}
